<?php
$GLOBALS[$GLOBALS['idx_lang']] = array(

	//A
	'activite_editoriale' =>'Activité éditoriale',
	'activite_editoriale_label' =>'Activité éditoriale - Paramètres des alertes',
	'article_pas_maj' => 'Votre attention est nécessaire, un article dont vous êtes l\'auteur n\'a pas été mis à jour depuis @jours@ jours.',
	
	//D
	'delai' => 'Délai',
	'derniere_maj' => 'Dernière MàJ',
	'du' => 'Du ',
	'date' => 'Date',
	
	//E
	'editer' => 'Éditer l\'article',
	'extras_delai_label' => 'Délai avant alerte',
	'extras_delai_explications' => 'À compter d\'une mise à jour, le nombre de jours avant d\'envoyer une alerte.',
	'extras_emails_label' => 'Email',
	'extras_emails_explications' => 'Les adresses emails auxquelles doivent parvenir les alertes, séparées par des virgules.',
	'extras_identifiants_label' => 'Identifiants des auteurs &agrave; alerter',
	'extras_identifiants_explications' => 'Séparés par des virgules, par exemple :"1,12"',
	'extras_frequence_label' => 'Fréquence de relance de l\'alerte',
	'extras_frequence_explications' => 'Si la mise à jour n\'a pas été faite, au bout de combien de jours l\'alerte sera à nouveau envoyée.',
	'explications_alerter_auteur' => 'Dans ce cas ne choisissez pas la date de mise à jour de la rubrique, cela interdit de prévenir les auteurs (une rubrique n\'a pas d\'auteur).',
	'explications_champ' => '<strong>La date de mise à jour de la rubrique</strong> : elle est évaluée à chaque publication d\'un élément et à chaque modification d\'un élément publié ou de la rubrique elle-même.<br />
	<strong>La date de modification des articles (et breves) de la branche</strong> (c\'est aussi la date de première publication).<br />
	<strong>La date de modification des articles (et breves) de la rubrique</strong> (c\'est aussi la date de première publication).',
	'explications_modifier_rubrique_pour_parametrer' => 'Modifiez la rubrique pour paramétrer les alertes.',
	
	//F
	'frequence_de_relance' => 'Fréquence de relance',
	
	//J
	'jours' => 'jour(s)',
	'jusqua' => 'Jusqu\'au',
	
	//L
	'label_alerter_auteur' => 'Alerter les auteurs des articles',
	'label_oui_alerter_auteur' => 'oui',
	'label_cfg_champ' => 'Quel est la date analysée pour évaluer le délais de mise à jour ?',
	
	//P
	'prevenir_responsable' => 'Vous êtes identifié comme responsable de l\'activité éditoriale de la rubrique “ @titre@ ”.',
	'prevenir_auteur' => 'Vous êtes identifié comme auteur d\'un article de la rubrique “ @titre@ ”.',
	
	//R
	'responsables' => 'Responsables',
	'rubriques_a_suivre' => 'Rubriques à suivre',
	'rubrique_doit_maj' => 'Une rubrique doit être mise a jour',
	'rubrique_pas_maj' => 'Votre attention est nécessaire, la rubrique n\'a pas été mise à jour depuis @jours@ jours.',
	'rubriques_pas_a_jour' => 'Rubriques n\'ayant pas été mises à jour à temps',
	'rubrique'			=> 'RUBRIQUE: ',
	
	//S
	'secteur' => 'Secteur:',
	
	//T
	'titre_message' => 'Ceci est un message automatique.',
	
	//CFG
	'cfg' => 'Configurer le plugin Activité éditoriale',
	'cfg_maj_rubrique' => 'La date de mise à jour de la rubrique',
	'cfg_date_modif_branche' => 'La date de modification des articles/breves de la branche',
	'cfg_date_modif_rubrique' => 'La date de modification des articles/breves de la rubrique',
	
);